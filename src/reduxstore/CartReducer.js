function CartReducer(state = {
    cart: [],
    totalprice: 0,
    itemsInCart: 0,
    totalItems: 0,
    
}, action) {
    switch (action.type) {
        case "ADDTOCART": {
            state = { ...state }
            // state["cart"] = state["cart"].push(action.payload)
            state["cart"] = [...state["cart"], action.payload]

            state["itemsInCart"] = state["itemsInCart"] + 1
            return state
        }
        case "EMPTYCART": {
            state = { ...state }

            return state
        }
        case "CARTITEMS": {
            state = { ...state }
            state.dbCartItems = action.payload
            return state
        }
        case "TOTALPRICE": {
            state = { ...state }
            state.totalPrice = action.payload
            return state
        }
        case "TOTALITEMS": {
            state = { ...state }
            state.totalItems = action.payload
            return state
        }


        case "REMOVEFROMCART": {
            state = { ...state }
            state.cart = []
            return state
        }
        default: return state
       
    }
    
}
export default CartReducer