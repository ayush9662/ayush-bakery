import { createStore, combineReducers } from 'redux'
import Authreducer from './Authreducer'
import CartReducer from './CartReducer'

var reducers = combineReducers({ Authreducer, CartReducer })
let store = createStore(reducers)
console.log(store)
export default store

