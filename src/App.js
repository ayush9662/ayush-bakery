import logo from './logo.svg';
import './App.css';
import Nav from './Components/Nav';
// import './slider.css';
import Carousel from './Components/Carousel';
import Signup from './Components/Signup';
import Card from './Components/Card';
import Data from './Components/Data'

import Home from './Components/Home'
import Login from './Components/Login'
import Register from './Components/Register'
import Cakedetail from './Components/Cakedetail'
import Search from './Components/Search'
import Cart from './Components/Cart'
import {BrowserRouter as Router,Route,Switch} from "react-router-dom"
import { connect } from 'react-redux';
function App() {
  var details={
    'projectname':'Ayush bakery',
    'username':'Ayush'
  }
  return (
    
    <div className="App">
      <Router>
      <Nav details={details}  phone="8400969503"></Nav>
      <Switch>
        <Route exact path="/" component={Home}></Route>
        <Route exact path="/cakes" component={Data}></Route>
        <Route exact path="/login" component={Login}></Route>
        <Route exact path="/signup" component={Register}></Route>
        {/* <Route path="/checkout" component={Checkout} /> */}
        <Route exact path="/cart" component={Cart}></Route>
        <Route exact path="/search" component={Search}></Route>
        <Route exact path="/cake/:cakeid" component={Cakedetail}></Route>
        <Route exact path="/signup" component={Register}></Route>
        </Switch>
      </Router>
      
      {/* <Carousel></Carousel>
      <Signup></Signup>
     <Data></Data> */}
      
    </div>
  );
}

export default connect()(App);
