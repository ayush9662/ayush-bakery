import axios from "axios";
import {useState} from "react";
import { Link,useHistory } from 'react-router-dom';
function Nav(props)
{
  var token=localStorage.token
 const [searchString,setSearchString]=useState('');
 let history = useHistory();
  function showalert(event)
  {
    event.preventDefault();
    if(searchString!='')
    {
      var url="/search?q="+searchString;
      history.push(url)
    }
    
    
  }
  function getSearchText(event)
  {
    setSearchString(event.target.value)
  }

  const [status,setStatus]=useState(true);
  

    return(
        <div>
<nav className="navbar navbar-expand-lg navbar-light bg-light">
  <Link to="/"><a className="navbar-brand" >{props.details.projectname}</a></Link>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item active">
        <a className="nav-link" href="#">{props.details.username} <span className="sr-only">(current)</span></a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="#">Link</a>
      </li>
      <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
          Dropdown
        </a>
        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
          <a className="dropdown-item" href="#">Action</a>
          <a className="dropdown-item" href="#">Another action</a>
          <div className="dropdown-divider"></div>
          <a className="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li className="nav-item">
        <a className="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li>
    </ul>
    <form className="form-inline my-2 my-lg-0">
      <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" onKeyUp={getSearchText}></input>
      <button className="btn btn-outline-success my-2 my-sm-0" type="submit" onClick={showalert}>Search</button>
    </form>
    
     {token?'Logout':(<Link to="/login">Login</Link> | 
     <Link to="/signup">Signup</Link>)
    }
  </div>
 
</nav>
        </div>)

    
}

export default Nav;