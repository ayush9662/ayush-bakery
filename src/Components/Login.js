import React, { useState } from 'react';
import axios from 'axios'

function Login(props) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [emailErr,setEmailErr]=useState('');
    const [passwordErr,setPasswordErr]=useState('');
    const [islogin,setLogin]=useState(false);

    function emailSet(e) {
        let item=e.target.value;
        let pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
                
        if(item.length==0)
        {
            setEmailErr('Please enter email address');
        }
        else if(!pattern.test(item)){
            setEmailErr('Please enter a valid email address');

        }
        else
        {
            setEmailErr('');
        }
        setEmail(item);
    }
    function passwordSet(e){
        let pass=e.target.value;
        if(pass.length==0){
            setPasswordErr('Please enter password');
        }
        else{
            setPasswordErr('');
        }
        setPassword(pass);
    }

    function loginHandle(e) {
        e.preventDefault();
         if(email==''){
            setEmailErr('Please enter a valid email')
        }
        else if(password==''){
            setPasswordErr('Please enter a six digit password')
        }
        else{
            axios({
                url: process.env.REACT_APP_BASE_URL+'/login',
                method:"post",
                data:{'email':email,'password':password}
            }).then((response)=>{
                if(response.data.message)
                {
                    alert(response.data.message)
                  
                }
                else{
                    // if(response.data.email){
                    //     props.dispatch({
                    //         type:"LOGIN",
                    //         payload:{
                    //             token:response.data.token
                    //         }
                    //     })
                    // }
                    localStorage.token= response.data.token
                    alert('Login Successfully')
                    setLogin(true)
                    
                    props.history.push('/')
                }
            },
            (error)=>{
                console.log(error)
            })
        }
    }
    return (
        <div className="container">
            <br></br>
            <h3> Login to your accout</h3>
            <br></br><br></br>
            <form onSubmit={loginHandle}>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <input type="email" className="form-control" onKeyUp={emailSet} id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                        </div>
                        {emailErr} 
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <input type="password" className="form-control" onKeyUp={passwordSet} id="exampleInputPassword1" placeholder="Password" />
                        </div>
                        {passwordErr} 
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <button type="submit" className="btn btn-primary">Login</button>
                    </div>
                </div>

            </form>
        </div>
    );
}
export default Login;