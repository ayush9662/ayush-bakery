import querystring from "query-string"
import axios from 'axios'
import { useEffect } from 'react';
import { useState } from 'react';
import Card from './Card';
function Search(props) {
   var [cake, setCake] = useState([]);
   var query = querystring.parse(props.location.search)
   

      //api call
      useEffect(() => {
         if (query.q != '') {
         axios({
            url: process.env.REACT_APP_BASE_URL + '/searchcakes?q=' + query.q,
            method: "get",
            data: JSON

         }).then((response) => {
            setCake(response.data.data);
         }, (error) => { })
      }
         else {
            alert('No search keyword found')
            props.history.push('/')
         }

      })
   
     


   return (
      <div>
         <h1> Search cakes for {query.q}</h1><hr></hr>
         <div class="container">
            <div className="row" style={{ marginLeft: "70px" }}>
               {
                  cake.map((each, index) => {
                     return (<Card data={each} key={index}></Card>)
                  })
               }
            </div>
         </div>



      </div>
   )
}

export default Search;




