import axios from 'axios'
import { useEffect, useState } from 'react'
import { connect } from 'react-redux';

function Cakedetail(props) {

    var cakeid = props.match.params.cakeid
    const [cakedetail, setCakeDetail] = useState('')

    function addToCart()
    {
       if(!localStorage.token){
           props.history.push('/login')
           return false;
       }
       axios({
           url: process.env.REACT_APP_BASE_URL+'/addcaketocart',
           method:"post",
           data: { 
               'cakeid':cakedetail.cakeid,
               'name': cakedetail.name, 
               'image': cakedetail.image,
               'price':cakedetail.price,
               'weight':cakedetail.weight
            },
            headers:{
                authtoken:localStorage.token
            }

       }).then((response)=>{
           props.dispatch({
               type:"ADDTOCART",
               payload:response.data
           })
           props.history.push('/cart')
           
       },(error)=>{})
      
    }
    useEffect(() => {
        if (cakeid != '') {
            axios({
                url: process.env.REACT_APP_BASE_URL + '/cake/' + cakeid,
                method: "get",
                data: JSON

            }).then((response) => {
                setCakeDetail(response.data.data);
               
            }, (error) => { })
        }
        else {
            alert('No search keyword found')
            setCakeDetail('');
        }

    }, [])

    

    return (
        <div className="container">
            <br></br>
            <h1>Cake Detail Page</h1>
            <div class="row">
                <div className="column" style={{ backgroundColor: "#aaa" }}>
                    <h2>{cakedetail.name}</h2>
                    <img src={cakedetail.image} style={{ height: "25rem",width:"35rem" }}></img>
                </div>
                <div className="column" style={{ backgroundColor: "#bbb", width: "35rem" }}>
                    <table class="table">

                        <tbody>
                            <tr>

                                <td>Rating</td>
                                <td>{cakedetail.ratings}</td>
                            </tr>

                            <tr>

                                <td>Price</td>
                                <td>{cakedetail.price}</td>
                            </tr>
                            <tr>
                                <td>Weight</td>
                                <td>{cakedetail.weight}</td>
                            </tr>
                            <tr>
                                <td>Flavour</td>
                                <td>{cakedetail.flavour}</td>
                            </tr>
                            <tr>
                                <td>Occasion</td>
                                <td>{cakedetail.type}</td>
                            </tr>

                        </tbody>
                    </table>
                    <button className="btn btn-success" onClick={addToCart}>Add to cart</button>
                </div>
            </div>
        </div>


    );

}

export default connect()(Cakedetail);