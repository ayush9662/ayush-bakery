import {Link} from 'react-router-dom'

function Card( props)
{
    if(props.data)
    {
        return(
        <Link to={'cake/' + props.data.cakeid}>
            <div className="card" style={{width: "18rem"}}>
      <img className="card-img-top" src={props.data.image} alt="Card image cap" style={{height:"15rem"}}></img>
      <div className="card-body">
        <h5 className="card-title">{props.data.name}</h5>
        <p className="card-text">{props.data.price}</p>
       
      </div>
    </div>
    </Link>
        )
    }
    else
    {
        return null
    }
    

}

export default Card;