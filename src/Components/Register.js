import { useState } from "react";
import axios from 'axios';

function Register(props)
{
    
    const [name,setName]=useState('');
    const [email,setEmail]=useState('');
    const [password,setPassword]=useState('');
    const [nameErr,setNameErr]=useState('');
    const [emailErr,setEmailErr]=useState('');
    const [passwordErr,setPasswordErr]=useState('');

    function nameHandle(e)
    {
        let nameCheck=e.target.value;
        if(nameCheck.length==0)
        {
            setNameErr('Please enter the name');
        }
        else{
            setNameErr('')
        }
        setName(nameCheck);
    }
    function emailHandle(e)
    {
        let emailCheck=e.target.value;
        let pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
         
        if(emailCheck.length==0)
        {
            setEmailErr('Please enter an email address')
        }
        else if(!pattern.test(emailCheck))
        {
            setEmailErr('Please enter a valid email address')
        }
        else{
            setEmailErr('');
        }
        setEmail(emailCheck)
    }
    function passwordHandle(e)
    {
        let passCheck=e.target.value
        if(passCheck.length<6)
        {
            setPasswordErr('Please enter a six digit password')
        }
        else{
            setPasswordErr('')
        }
        setPassword(passCheck)
    }

    function formHandle(e)
    {
        e.preventDefault();
        if(name==''){
            setNameErr('Please enter the name')

        }
        else if(email==''){
            setEmailErr('Please enter a valid email')
        }
        else if(password==''){
            setPasswordErr('Please enter a six digit password')
        }
        else{
            //api call
            axios({
				url: process.env.REACT_APP_BASE_URL+'/register',
				method: "post",
				data: { 'name':name,'password': password, 'email': email }
			}
			).then((response) => {
				console.log(response.data.message);
				if (response.data.message){
                    alert(response.data.message);
                    props.history.push('/login');
                }        
				else {
					alert("Signup failed");
				}
			}, (error) => {
				console.log(error);

			});
        }
    }
    return (
        <div className="container">
            <br></br>
            <h3> Signup to your accout</h3>
            <br></br><br></br>
            <form onSubmit={formHandle}>
            <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <input type="text" className="form-control" onKeyUp={nameHandle} id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name" />
                        </div>
                        {nameErr}
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <input type="email" className="form-control" onKeyUp={emailHandle} id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                        </div>
                        {emailErr}
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <input type="password" className="form-control" onKeyUp={passwordHandle} id="exampleInputPassword1" placeholder="Password" />
                        </div>
                        {passwordErr}
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <button type="submit" className="btn btn-primary">Signup</button>
                    </div>
                </div>

            </form>
        </div>
    );
}

export default Register;