import { Link, withRouter } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
//import Cake from "./Components/Cake";
import { connect } from "react-redux";


function Cart(props) {
    const [cakes, getCakes] = useState([]);
    const [totalPrice, setTotalPrice] = useState(0);
    const [totalItem, setTotalItem] = useState(0);


    useEffect(() => {
        if (!localStorage.token) {
            props.history.push('/login')
            return false;
        }
        axios({
            url: process.env.REACT_APP_BASE_URL + '/cakecart',
            method: 'post',
            headers: {
                authtoken: localStorage.token
            }
        }).then((response) => {
            getCakes(response.data.data)
            props.dispatch({
                type: "CARTITEMS",
                payload: response.data.data
            })
           
            var total = 0;
            var totalItems = 0;
            response.data.data.forEach(element => {
                total += element.price;
                totalItems += 1;
            });
            console.log(total)
            console.log(totalItems)
            props.dispatch({
                type: "TOTALPRICE",
                payload: total
            })
            props.dispatch({
                type: "TOTALITEMS",
                payload: totalItems
            })

            setTotalPrice(total)
            setTotalItem(totalItems)
        }, (error) => {
            console.log('error')
        })
    }, [localStorage.token])

    function removeItem (cakeid){
     
        axios({
            url: process.env.REACT_APP_BASE_URL + "/removecakefromcart",
            method: "POST",
            headers: { authtoken: localStorage.token },
            data: { cakeid: cakeid }
        }).then((response) => {
            props.history.push("/cart")
        },(error)=>{})
    }
    // const emptyCart = () => {
    //     props.dispatch(emptyCartMiddleware())
    // }

    return (
        <div class="table-responsive"><br></br><br></br>
            <span><b>Dear User, You have total {totalItem} items worth {totalPrice}</b></span>
            <table class="table">
                <thead>
                    <tr>
                        <th colspan="2">Cake</th>
                        <th>Quantity</th>
                        <th> price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    {cakes.map((each, index) => {
                        //setTotalPrice(totalPrice)
                        return (
                            <tr key={index}> 
                                <td><img src={each.image} style={{width:"15rem",height:"15rem"}}/></td>
                                <td>{each.name}</td>
                                <td>
                                    {each.quantity}
                                </td>
                                <td>${each.price}</td>
                                <td><a onClick={removeItem(each.cakeid)} ><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                        )
                    })}
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="3">Total</th>
                        <th colspan="2">${totalPrice}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    )
}

export default connect()(withRouter(Cart))