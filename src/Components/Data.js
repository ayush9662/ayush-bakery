import Card from './Card';
import axios from 'axios';
import { useEffect } from 'react';
import {useState} from 'react';

function Data() {

    var [cake, setCake] = useState([]);
        useEffect(()=>{
    axios({
        method:"get",
        url:process.env.REACT_APP_BASE_URL+"/allcakes",
        data:JSON
    }).then((response)=>{
        setCake(response.data.data);
    },(error)=>{})
        },[]);
   
    
    return (
        <div class="container">
            <div className="row" style={{ marginLeft: "70px" }}>
                {
                    cake.map((each, index) => {
                        return (<Card data={each} key={index}></Card>)
                    })
                }
            </div>
        </div>
        
    )


}

export default Data;