import React from 'react';
class Signup extends React.Component{
    constructor()
    {
        super();
        this.state=
        {
            email:null,
            emailErr:null
        }
    }
    emailSet=(e)=>{
        let item=e.target.value;
        this.setState(
            {
                email:item
            }
        )
    }

     emailCheck=(event)=>
        {
            event.preventDefault();
            if(this.state.email==null)
            {
                alert('Enter email id')
                this.setState({
                    emailErr:'Please enter email id'
                })
            }
            else if(!(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.state.email)))
 
            {

                alert('Enter a valid email id')
                this.setState({
                    emailErr:'Please enter a valid email id'
                })
            }
            else
            {
                alert('email is ok')
                this.setState({
                    emailErr:' '
                })
            }
           
            
        }
    render()
    {
        
        return(
            <div>
                <form onSubmit={this.emailCheck}>
                    <input type="text" placeholder="Enter your email" onKeyUp={this.emailSet}></input>
                  <span>{this.state.emailErr}</span>
                    <br></br><br></br>
                    <button type="submit">Check Email</button>
                </form>
            </div>
        )
    }
}

export default Signup;